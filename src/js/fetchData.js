const api = 'https://hacker-news.firebaseio.com/v0/'

export async function* topStoriesGenerator(storyIdx = 0) {
    const result = await fetch(`${api}topstories.json`)
    const stories = await result.json()
    while (storyIdx < stories.length) {
        const storyIds = stories.slice(storyIdx, storyIdx + 10)
        const items = await Promise.all(storyIds.map(getItem))
        yield items
        storyIdx = storyIdx + 10
    }
}

export async function getItem(itemId) {
    const result = await fetch(`${api}/item/${itemId}.json`)
    const item = await result.json()
    return item
}
