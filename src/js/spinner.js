const spinner = document.querySelector('.js-spinner')

export function showSpinner() {
    spinner.classList.add('show')
}
export function hideSpinner() {
    spinner.classList.remove('show')
}
