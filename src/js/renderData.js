import { topStoriesGenerator, getItem } from './fetchData.js'
import { showSpinner, hideSpinner } from './spinner.js'
import './HNItem.js'

const storiesContainer = document.querySelector('.js-stories')
const commentsContainer = document.querySelector('.js-comments')

const storyGenerator = topStoriesGenerator()

export async function renderTopStories() {
    showSpinner()
    const { value } = await storyGenerator.next()
    const storiesHtml = value.map(renderItem).join('')
    storiesContainer.insertAdjacentHTML('beforeend', storiesHtml)
    hideSpinner()
    storyEvents()
}

function renderItem({ title = '', text = '', by = '', score = null, descendants = null, kids = [], subItem = false }) {
    const commentIds = kids.join(',')
    const commentText = text.replace(/"/g, '&quot;')
    const subItemClass = subItem ? 'sub-item' : ''
    return `
        <hn-item
            class="js-hn-item hn-item ${subItemClass}"
            title="${title}"
            text="${commentText}"
            by="${by}"
            score="${score}"
            descendants="${descendants}"
            data-comments="${commentIds}"
        ></hn-item>`
}

async function renderComments(commentId = [], subItem = false) {
    showSpinner()
    const comment = await getItem(commentId)
    if (subItem) comment.subItem = true
    if (comment.kids) comment.kids.forEach(kidId => renderComments(kidId, true))
    const commentHtml = renderItem(comment)
    commentsContainer.insertAdjacentHTML('beforeend', commentHtml)
    hideSpinner()
}

function storyEvents() {
    const stories = [...document.querySelectorAll('.js-hn-item')]

    const setActiveStory = story => {
        stories.forEach(s => s.classList.remove('active'))
        story.classList.add('active')
    }

    const handler = ({ target }) => {
        commentsContainer.classList.add('active')
        commentsContainer.innerHTML = ''
        setActiveStory(target)
        const commentIds = [...target.dataset.comments.split(',')]
        commentIds.forEach(renderComments)
    }
    const listener = story => story.addEventListener('click', handler)

    stories.forEach(listener)
}
