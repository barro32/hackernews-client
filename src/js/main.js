import { renderTopStories } from './renderData.js'

const loadMoreBtn = document.querySelector('.js-load-more-stories')
loadMoreBtn.addEventListener('click', () => renderTopStories())

renderTopStories()
