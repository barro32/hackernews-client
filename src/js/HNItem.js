class HNItem extends HTMLElement {
    constructor() {
        super()

        const spacing = '12px'

        const shadow = this.attachShadow({ mode: 'open' })

        const Item = document.createElement('div')
        const line1 = document.createElement('div')
        const line2 = document.createElement('div')
        const title = document.createElement('span')
        const by = document.createElement('span')
        const score = document.createElement('span')
        const comments = document.createElement('span')

        const style = document.createElement('style')

        Item.setAttribute('class', 'item')
        line1.setAttribute('class', 'item__line-1')
        line2.setAttribute('class', 'item__line-2')

        title.innerHTML = this.getAttribute('title') || this.getAttribute('text')
        by.textContent = `by ${this.getAttribute('by')}`
        score.textContent = this.getAttribute('score') !== 'null' ? `score ${this.getAttribute('score')}` : ''
        comments.textContent = this.getAttribute('descendants') !== 'null' ? `comments ${this.getAttribute('descendants')}` : ''

        style.textContent = `
            .item__line-1 {
                margin-bottom: calc(${spacing} * 0.5);
            }
            .item__line-1 > span {
                font-size: 18px;
            }
            .item__line-2 > span {
                font-size: 14px;
                margin-right: ${spacing}
            }
        `

        shadow.appendChild(Item)
        shadow.appendChild(style)
        Item.appendChild(line1)
        Item.appendChild(line2)
        line1.appendChild(title)
        line2.appendChild(by)
        line2.appendChild(score)
        line2.appendChild(comments)
    }
}

customElements.define('hn-item', HNItem)
