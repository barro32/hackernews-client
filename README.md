# HackerNews Top Stories Client

HackerNews client using the [HN API](https://github.com/HackerNews/API).  
Front end is written in vanilla JS.  

It will only run in modern browsers as a lot of modern features are being used, such as:
- Custom HTML Elements
- Async Generators
- Custom CSS Properties
- JS Modules

These features could be transpiled / polyfilled but i decided for the limited purpose of this app that it wasn't necessary.

## Running the app

There are zero dependencies, just open the `index.html` and everything _should_ work!  
Or view it here [https://barro32.gitlab.io/hackernews-client/]

### More info

The app will fetch and display the top 10 stories from HackerNews in the left pane.  
You can load the next 10 by clicking the "Load more" button.  
Clicking on a story will display it's comments in the right pane.

### Future improvemnts

- Styling!!
- Fetching and nesting comments properly
- Caching requests
- ~~Hide comments pane when not in use~~
